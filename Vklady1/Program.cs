﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vklad1;

namespace Vklady1
{
    class Program
    {
        static void vvod()
        {
            Console.Write("Ввдедите сумму :  ");
            double _sum = Double.Parse(Console.ReadLine());
            if (_sum < 0) throw new ArgumentOutOfRangeException("Число не должно быть отрицательным!");
            Console.Write("Ввдедите процентную ставку :  ");
            double _pr = Double.Parse(Console.ReadLine());
            if (_pr < 0) throw new ArgumentOutOfRangeException("Число не должно быть отрицательным!");
            Console.Write("Ввдедите срок вклада :  ");
            int _srok = Int32.Parse(Console.ReadLine());
            if (_srok < 1 ) throw new ArgumentOutOfRangeException("Число должно быть больше или равно единицы!");

            vklad n = new vklad(_sum, _pr, _srok);

            Program.Output(_srok, n.raschet());
 
        }
        static void Main(string[] args)
        {
            string a = string.Empty;
            string b = string.Empty;
            do
            {
                try
                {
                    vvod();
                }
                catch (ArgumentOutOfRangeException ex)
                {
                    Console.WriteLine(ex.Message);
                }
                catch (Exception)
                {
                    Console.WriteLine("Введите число!");
                }
                finally
                {
                    Console.WriteLine("Нажмите Y чтобы продолжить.");
                }
                Console.ReadLine();

        }
            while (a != "y");
            
        }
        
       static public void Output(int _srok, double[] Array)
        {
           for (int i = 1; i < _srok; ++i)
            {
                Console.WriteLine(Array[i]);
            }
           
        }
    }
}

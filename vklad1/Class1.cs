﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vklad1
{
    public class vklad
    {
        public double _sum;
        public double _pr;
        public int _srok;

        public vklad(double _sum, double _pr, int _srok)
        {
            this._sum = _sum;
            this._pr = _pr;
            this._srok = _srok;
        }

        public double[] raschet()
        {
            double[] array = new double[_srok];

            array[0] = _sum;
            
            for (int i = 1; i < _srok; i++)
            {
                double m = _sum * (_pr / _srok);
                array[i] = array[i-1] + m;
                array[i] = _sum + m*(i + 1);
            }
            return array;
        }
    }
}
